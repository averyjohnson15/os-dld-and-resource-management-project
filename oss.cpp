/***************************************************************************************************
Created by: Avery Johnson
Course/Assignment: CS4760/Project 5
Due Date: 20230425

Notes: See README or help documentation through option -h for further information.

Sources (aside from textbook or man pages):
- None
****************************************************************************************************/

//preprocessor directives
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <signal.h>
#include <sys/types.h>
#include <mqueue.h>
#include <ctime>
#include <queue>
#include <vector>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <cmath>
#include <math.h>
#include "resources.h"

using namespace std;

//declare struct prior to prototypes since its used as param
struct PCB
{
	int occupied;
	int pid;
	int startSec;
	int startNano;
	int r0;
	int r1;
	int r2;
	int r3;
	int r4;
	int r5;
	int r6;
	int r7;
	int r8;
	int r9;
	int blocked;
};

//struct for sec/nano used in blocked/ready vector totals
struct timePassed
{
	int sec;
	int nano;
};

//typedef and macro for sysV mq
#define PERMS 0644
typedef struct msgbuffer
{
	long mtype;
	int intData;
	int pid;
} msgbuffer;

//global process tree to be used in signal functions, global ints for mq and file line counter
struct PCB processTable[18];
int msqid;
int fileLineCount;
vector<double> allTermPer; //vector to keep track of all term percentages through DLD/DLR

//prototypes for funcs labeled below main (some funcs below for organizational purposes)
void printProcTable(struct PCB procTable[], int sec, int nano, FILE* logFile);
void removeProcess(struct PCB procTable[], int pid);
void addProcess(struct PCB procTable[], int pid, int sec, int nano);

//function to detect if a deadlock exists
queue<int> detectDeadlock(struct PCB procTable[], struct resources recs)
{
        //declare variables for deadlock termination percentage
        int totalProcs = 0; //total processes entering DLD for this iteration
        int totalTerm = 0; //total processes term'd through DL this iteration
        double localTermPer = 0; // percentage of processes term'd, used in avg later

	//declare return queue and create temp processTable and tempResources for evalulation
	queue<int> deadlockedProcs;
	struct PCB tempTable[18];
	for (int i = 0 ; i < 18 ; i++)
	{
		if (procTable[i].occupied == 1)
			totalProcs++;
		tempTable[i] = procTable[i];
	}
	struct resources tempRecs = recs;


	//remove any nonblocked processes from the tempTable and add the resources back to tempPool
	for (int i = 0 ; i < 18 ; i++)
	{
		if (procTable[i].blocked == -1 && procTable[i].occupied == 1)
		{
			int tempPid = procTable[i].pid;
			removeProcess(tempTable, tempPid);
			tempRecs.R0 += procTable[i].r0;
			tempRecs.R1 += procTable[i].r1;
                        tempRecs.R2 += procTable[i].r2;
                        tempRecs.R3 += procTable[i].r3;
                        tempRecs.R4 += procTable[i].r4;
                        tempRecs.R5 += procTable[i].r5;
                        tempRecs.R6 += procTable[i].r6;
                        tempRecs.R7 += procTable[i].r7;
                        tempRecs.R8 += procTable[i].r8;
                        tempRecs.R9 += procTable[i].r9;
		}
	}
	//loop through and see how many blocked process there are so we know how many iterations are needed
	int blockedProcs = 0;
	for (int i = 0 ; i < 18 ; i++)
	{
		if (tempTable[i].occupied == 1)
			blockedProcs++;
	}

	//if there are still any blocked processes left, loop through tempTable and remove processes when possible, remaining are DL'd
	if (blockedProcs >= 2)
	{
		//loop through the tempTable for the number of blocked procs. If any remain after this, those are DL'd	
		for (int i = 0 ; i < blockedProcs ; i++)
		{
			for (int i = 0 ; i < 18 ; i++)
			{
				bool foundFlag = false;
				switch (tempTable[i].blocked)
				{
					case -1:
						break;
					case 0:
						if (tempRecs.R0 > 0)
							foundFlag = true;
						break;
					case 1:
						if (tempRecs.R1 > 0)
							foundFlag = true;
						break;
					case 2:
						if (tempRecs.R2 > 0)
							foundFlag = true;
						break;
					case 3:
						if (tempRecs.R3 > 0)
							foundFlag = true;
						break;
					case 4:
						if (tempRecs.R4 > 0)
							foundFlag = true;
						break;
					case 5:
						if (tempRecs.R5 > 0)
							foundFlag = true;
						break;
					case 6:
						if (tempRecs.R6 > 0)
							foundFlag = true;
						break;
					case 7:
						if (tempRecs.R7 > 0)
							foundFlag = true;
						break;
					case 8:
						if (tempRecs.R8 > 0)
							foundFlag = true;
						break;
					case 9:
						if (tempRecs.R9 > 0)
							foundFlag = true;
						break;
				}
				if (foundFlag)
				{
                        		tempRecs.R0 += tempTable[i].r0;
                        		tempRecs.R1 += tempTable[i].r1;
                        		tempRecs.R2 += tempTable[i].r2;
                        		tempRecs.R3 += tempTable[i].r3;
                        		tempRecs.R4 += tempTable[i].r4;
                        		tempRecs.R5 += tempTable[i].r5;
                        		tempRecs.R6 += tempTable[i].r6;
                        		tempRecs.R7 += tempTable[i].r7;
                        		tempRecs.R8 += tempTable[i].r8;
                        		tempRecs.R9 += tempTable[i].r9;
					int tempPid = tempTable[i].pid;
					removeProcess(tempTable, tempPid);
					break;
				}
			}
		}
		
		//if only 1 process, alg has failed. Throw error.
		if (tempTable[0].occupied == 1 && tempTable[1].occupied == 0)
		{
			printf("issue running DL alg, only 1 blocked proc left.\n");
			exit(1);
		}
		//if none are occupied, there are no blocked processes found through the algorithm left so no deadlock. Return empty queue
		else if (tempTable[0].occupied == 0)
		{
			return deadlockedProcs;
		}
		//otherwise, add the pids of the items left to the deadlocked procsQ, these are the ones left and they're deadlocked
		else
		{
			for (int i = 0 ; i < 18 ; i++)
			{
				if (tempTable[i].occupied == 1)
					deadlockedProcs.push(tempTable[i].pid);
			}
			//if deadlock is found, set total term'd locally and calculate average locally, add to vector for later avg
			totalTerm = deadlockedProcs.size();
			localTermPer = (double)totalTerm / totalProcs;
			allTermPer.push_back(localTermPer);
		}
	}
	return deadlockedProcs;
}


int findPos(struct PCB procTable[], int pid)
{
	int pos = -1;
	for (int i = 0 ; i < 18 ; i++)
	{
		if (procTable[i].pid == pid)
		{
			pos = i;
			break;
		}
	}
	if (pid == -1)
	{
		printf("issue finding pid pos\n");
		exit(1);
	}
	else
		return pos;
}

//function to remove relative proc from a blockedQ
queue<int> clearBlockedProcess(queue<int> blockedQ, int pid)
{
	queue<int> temp;

	while (blockedQ.size() > 0)
	{
		if (blockedQ.front() != pid)
			temp.push(blockedQ.front());
		blockedQ.pop();
	}
	return temp;
}

//func to call worker
void callWorker()
{
        char* args [] = {"user_proc", 0};
        execv("./user_proc", args);
}

//func to increment time (just for organization)
void incrTime(int* sec, int* nano)
{
	*nano += 1000000;
	if (*nano >= 1000000000)
	{
		*nano -= 1000000000;
		(*sec)++;
	}
}

//func that states what to run once SIGINT is caught (ctrl + c)
void sigCatcher(int sig)
{
	//unlink shared mem
	shm_unlink("/secMemSeg");
	shm_unlink("/nanoMemSeg");

        //get rid of sysV msq
        if (msgctl(msqid, IPC_RMID, NULL) == -1)
        {
                printf("issue killing mq on parentside\n");
                exit(1);
        }

	//terminate entire group
	killpg(getpid(), SIGTERM);
}

//code that was given to us for limiting process interval
//this func states what actually happens
static void myhandler(int s) 
{
	//unlink shared mem
	shm_unlink("/secMemSeg");
	shm_unlink("/nanoMemSeg");


        //get rid of sysV msq
        if (msgctl(msqid, IPC_RMID, NULL) == -1)
        {
                printf("issue killing mq on parentside\n");
                exit(1);
        }

	//terminate entire group
	killpg(getpid(),SIGTERM);
}
//this func establishes the interrupt
static int setupinterrupt(void) 
{
	struct sigaction act;
	act.sa_handler = myhandler;
	act.sa_flags = 0;
	return (sigemptyset(&act.sa_mask) || sigaction(SIGPROF, &act, NULL));
}
//this func establishes the timer, started at max 2 seconds, changed to 60 per requirements
static int setupitimer(void) 
{
	struct itimerval value;
	value.it_interval.tv_sec = 60;
	value.it_interval.tv_usec = 0;
	value.it_value = value.it_interval;
	return (setitimer(ITIMER_PROF, &value, NULL));
}

//entry point
int main(int argc, char* argv[])
{
	//declare/define vars for statistics
	int grantedI = 0;//requests granted immediately
	int grantedW = 0;//requests granted after waiting
	int termD = 0;//amount of processes term'd through deadlock detection/recovery
	int termS = 0;//amount of processes term'd successfully of own volition
	int timesR = 0;//times deadlock detection/recovery ran
	double avgTermPer = 0;//Average percentage of processes need to term on average in deadlock
	int requestsGranted = 0;//totalnum of requests granted

	//init resources struct and give all resources max value of 20
	struct resources recs;
	recs.R0 = RESOURCE_MAX;
	recs.R1 = RESOURCE_MAX;
	recs.R2 = RESOURCE_MAX;
	recs.R3 = RESOURCE_MAX;
	recs.R4 = RESOURCE_MAX;
	recs.R5 = RESOURCE_MAX;
	recs.R6 = RESOURCE_MAX;
	recs.R7 = RESOURCE_MAX;
	recs.R8 = RESOURCE_MAX;
	recs.R9 = RESOURCE_MAX;

	//define init value for line count
	fileLineCount = 0;

	//calls for error handling in relation to setting the program interval
	if (setupinterrupt() == -1)
	{
		printf("Error setting up interrupt handler, terminating...\n");
		exit(1);
	}
	if (setupitimer() == -1)
	{
		printf("Error setting up interval timer, terminating...\n");
		exit(1);
	}
	//establish what happens if user tries ctrl + c
	signal(SIGINT, sigCatcher);

	//declare blocked queues for each resource (r1bq = blockedqueue for resource 1)
	queue<int> r0bq;
	queue<int> r1bq;
	queue<int> r2bq;
	queue<int> r3bq;
	queue<int> r4bq;
	queue<int> r5bq;
	queue<int> r6bq;
	queue<int> r7bq;
	queue<int> r8bq;
	queue<int> r9bq;

	//declare resources for mq and touch mq file
	msgbuffer sndBuffer, rcvBuffer;
	key_t key;
	system("touch msgq.txt");

	//ftok to get key for message queue
	if ((key = ftok("msgq.txt", 1)) == -1)
	{
		printf("ftok issue on parentside\n");
		exit(1);
	}	

	//set up msg queue
	if ((msqid = msgget(key, PERMS | IPC_CREAT)) == -1)
	{
		printf("issue with msgget in parent\n");
		exit(1);
	}

	//fill process tree with zeros
	for (int i = 0; i < 18 ;i++)
	{
		processTable[i].occupied = 0;
		processTable[i].pid = 0;
		processTable[i].startSec = 0;
		processTable[i].startNano = 0;
		processTable[i].r0 = 0;
		processTable[i].r1 = 0;
		processTable[i].r2 = 0;
		processTable[i].r3 = 0;
		processTable[i].r4 = 0;
		processTable[i].r5 = 0;
		processTable[i].r6 = 0;
		processTable[i].r7 = 0;
		processTable[i].r8 = 0;
		processTable[i].r9 = 0;
		processTable[i].blocked = -1;
	}

        //declare opt, but define default values for opt args
        int opt;
	string logString = "defaultLog";
	bool verboseCheck = false;

        //loop through command line entry until no opts/args remain
        while ((opt = getopt(argc, argv, "hvf:")) != -1)
        {
                switch (opt)
                {
                        //incase of help option, show user the help message.
                        case 'h':
                                printf("\n!OSS Help! (Check README for further help information)\n\n");
                                printf("-Please use './oss' for envocation.\n-If you want to set the logFile that the output will be pushed to, an option will need to be used.\n");
                                printf("-An example command is 'oss -f myLog', the command outputs the 'oss' output to the terminal and to the logfile, 'myLog'.\n");
                                printf("\nFor clarity, the options and their meanings are listed below (option -f needs an additional argument):\n");
                                printf("-option '-h' envokes the help portion.\n");
				printf("-option '-f' and its following argument specify the logFile that 'oss' pushes its output to.\n");
				printf("-option '-v' stipulates a 'verbose' logging functionality. Otherwise, the default is non-verbose.\n");
                                exit(0);
                        //if proper opt other than -h is used, set the relative variable to the value that is input at the relative arg position
			case 'f':
				logString = optarg;
				break;
			case 'v':
				verboseCheck = true;
				break;
                        default:
                                printf("ERROR: An incorrect option was entered, or an argument is missing after an option that expects an argument!\n");
                                exit(1);
                }
        }
	//make FILE* from logString
	FILE* logFile = fopen(logString.c_str(), "w");

	//create shared mem sec and nano vars
	//set up sec destination, throw error if issue occurs
	int secMemSeg = shm_open("/secMemSeg", O_RDWR|O_CREAT, 0777);
	if (secMemSeg < 0)
	{
		printf("issue creating sec mem seg\n");
		exit(1);
	}
	//configure size of sec mem seg
	ftruncate(secMemSeg, sizeof(int));
	//use mmap to map sec address, throw relative error if issue occurs
	void* secAddress = mmap(NULL, sizeof(int), PROT_READ|PROT_WRITE, MAP_SHARED, secMemSeg, 0);
	if (secAddress == MAP_FAILED)
	{
		printf("issue setting up sec address\n");
		exit(1);
	}
	//set that address as the value of a pointer to the sec int value
	int* sysSec = (int*)secAddress;
	*sysSec = 0;

	//set up nano mem seg, throw error if issue occurs
	int nanoMemSeg = shm_open("/nanoMemSeg", O_RDWR|O_CREAT, 0777);
	if (nanoMemSeg < 0)
	{
		printf("Issue creating nano mem seg\n");
		exit(1);
	}
	//configure size of nano mem seg
	ftruncate(nanoMemSeg, sizeof(int));
	//use mmap to map nano address, throw relative error if issue occurs
	void* nanoAddress = mmap(NULL, sizeof(int), PROT_READ|PROT_WRITE, MAP_SHARED, nanoMemSeg, 0);
	if (nanoAddress == MAP_FAILED)
	{
		printf("issue setting up nano address\n");
		exit(1);
	}
	//set shared address as value of pointer to the nano int value
	int* sysNano = (int*)nanoAddress;
	*sysNano = 0;

	//ints to keep track of how many children have been terminated, how many are currently running and have been launched
	int terminatedChildren = 0;
	int launchedChildren = 0;
	int maxWorkers = 40;
	bool maxTimeFlag = false;
	
	//seed the rng and create max time between sec and nano before starting primary loop
	srand(time(NULL));
	int mtbSec = 0;
	int mtbNano = 500000000;

	//create nextTimeFor and nextInterval vars for new process and set initial value
	//manually setting init values to 0 so we start out with 1 process able to run
	int niSec = rand() % (mtbSec + 1);
	int niNano = 0;
	if (niSec == mtbSec)
		niNano = rand() % (mtbNano + 1);
	else
		//should be (999999999 + 1) if mtbSec is above 0
		niNano = rand() % (500000000 + 1);
	int ntfSec = 0;//niSec;
	int ntfNano = 0;//niNano;

	//set start time for 
	time_t loopStart = time(0);

	//last printed sec, last DL/DR ran
	int lastDetect = -1;
	int lastGranted = 0;

	//primary sys loop, stop looping when all necessary children have terminated
	while (terminatedChildren < maxWorkers)
	{
		//check every loop if we need to cap max workers
		if (!maxTimeFlag && time(0) - loopStart >= 5)
		{
			maxTimeFlag = true;
			maxWorkers = launchedChildren;
		}		

		//increment time every loop
		incrTime(sysSec, sysNano);

		//print the procTable every sysSec along with current resources
		if (requestsGranted >= (lastGranted + 20))
		{
			lastGranted += 20;
			if (verboseCheck)
			{
				printf("MASTER: At %d sec %d nano CURRENTLY AVAILABLE RESOURCES: R0=%d R1=%d R2=%d R3=%d R4=%d R5=%d R6=%d R7=%d R8=%d R9=%d\n", *sysSec, *sysNano, recs.R0, recs.R1, recs.R2, recs.R3, recs.R4, recs.R5, recs.R6, recs.R7, recs.R8, recs.R9);
				fprintf(logFile, "MASTER: At %d sec %d nano CURRENTLY AVAILABLE RESOURCES: R0=%d R1=%d R2=%d R3=%d R4=%d R5=%d R6=%d R7=%d R8=%d R9=%d\n", *sysSec, *sysNano, recs.R0, recs.R1, recs.R2, recs.R3, recs.R4, recs.R5, recs.R6, recs.R7, recs.R8, recs.R9);
			}
			printProcTable(processTable, *sysSec, *sysNano, logFile);
		} 

		//generate new proc if proc needs launched and enough time has passed
		if ((*sysSec > ntfSec || *sysSec == ntfSec && *sysNano >= ntfNano) && launchedChildren < maxWorkers && processTable[17].occupied == 0)
		{
			launchedChildren++;
			//roll new interval/next times
			if (niSec == mtbSec)
				niNano = rand() % (mtbNano + 1);
			else
				niNano = rand() % (500000000 + 1);
			ntfSec = (*sysSec) + niSec;
			ntfNano = (*sysNano) + niNano;
			if (ntfNano >= 1000000000)
			{
				ntfSec++;
				ntfNano -= 1000000000;
			}
			
			//increase count for Lchildren and fork/exec child
			int forkPid = fork();
			//create worker if child
			if (forkPid == 0)
				callWorker();
			else if(forkPid == -1)
			{
				printf("Issue Forking\n");
				exit(1);
			}
			else
			{
				//add to table
				addProcess(processTable, forkPid, *sysSec, *sysNano);
			}
		}
		//init intData with a -1 to check if a message was actually received or running old value at each rcv
		rcvBuffer.intData = -1;

		//check if any messages need to be received by parentside
		if (msgrcv(msqid, &rcvBuffer, sizeof(msgbuffer), getpid(), IPC_NOWAIT) == -1)
		{
			if (errno != ENOMSG)
			{
				printf("issue receiving parentside\n");
				exit(1);
			}
		}

		//if intData is -10 to -19, worker is trying to get r0-r9, if 10 to 19, it's trying to return one. A -1 means no msg, 0 means self term, else there's a problem(shouldn't happen)
		//first see if it's a -1, meaning no msg was rcv'd
		if (rcvBuffer.intData == -1)
		{
			if (verboseCheck)
			{
				fprintf(logFile, "No Message rcv'd parentside (after check)\n");
				printf("No Message rcv'd parentside (after check)\n");
			}
		}
		//if msg is rcv'd then get pid pos in procTable and proceed accordingly
		else
		{
			//for testing, define sndBuffer intdata as -1
			sndBuffer.intData = -1;
			sndBuffer.mtype = rcvBuffer.pid;
			int pos = findPos(processTable, rcvBuffer.pid);
			//then check if intData is a request for resource. If so, grant request immediately if possible, otherwise add to relative blockedQ
			if (rcvBuffer.intData >= -19 && rcvBuffer.intData <= -10)
			{
				//reset sndBuffer intData to -1 to check later if we need to send a message or let worker stay blocked temporarily
				//check if a relative resource is available, if so, set sndBuffer to relative value and send, otherwise add to relative blockedQ
				switch (rcvBuffer.intData)
				{
					case -10:
						if (recs.R0 > 0)
							sndBuffer.intData = -10;
						else
						{
							r0bq.push(rcvBuffer.pid);
							processTable[pos].blocked = 0;
						}
						break;
					case -11:
                                                if (recs.R1 > 0)
                                                        sndBuffer.intData = -11;
                                                else
						{
                                                        r1bq.push(rcvBuffer.pid);
							processTable[pos].blocked = 1;
						}
                                                break;
					case -12:
                                                if (recs.R2 > 0)
                                                        sndBuffer.intData = -12;
                                                else
						{
                                                        r2bq.push(rcvBuffer.pid);
							processTable[pos].blocked = 2;
                                                }
						break;
					case -13:
                                                if (recs.R3 > 0)
                                                        sndBuffer.intData = -13;
                                                else
						{
                                                        r3bq.push(rcvBuffer.pid);
							processTable[pos].blocked = 3;
                                                }
						break;
					case -14:
                                                if (recs.R4 > 0)
                                                        sndBuffer.intData = -14;
                                                else
						{
                                                        r4bq.push(rcvBuffer.pid);
							processTable[pos].blocked = 4;
                                                }
						break;
					case -15:
                                                if (recs.R5 > 0)
                                                        sndBuffer.intData = -15;
                                                else
						{
                                                        r5bq.push(rcvBuffer.pid);
							processTable[pos].blocked = 5;
                                                }
						break;
					case -16:
                                                if (recs.R6 > 0)
                                                        sndBuffer.intData = -16;
                                                else
						{
                                                        r6bq.push(rcvBuffer.pid);
							processTable[pos].blocked = 6;
                                                }
						break;
					case -17:
                                                if (recs.R7 > 0)
                                                        sndBuffer.intData = -17;
                                                else
						{
                                                        r7bq.push(rcvBuffer.pid);
							processTable[pos].blocked = 7;
						}
                                                break;
					case -18:
                                                if (recs.R8 > 0)
                                                        sndBuffer.intData = -18;
                                                else
						{
                                                        r8bq.push(rcvBuffer.pid);
							processTable[pos].blocked = 8;
						}
                                                break;
					case -19:
                                                if (recs.R9 > 0)
                                                        sndBuffer.intData = -19;
                                                else
						{
                                                        r9bq.push(rcvBuffer.pid);
							processTable[pos].blocked = 9;
						}
                                                break;
				}
				if (sndBuffer.intData != -1)
				{
                                	if (msgsnd(msqid, &sndBuffer, sizeof(msgbuffer) - sizeof(long), 0) == -1)
                                	{
                                	        printf("sending issue from parentside\n");
                                	        exit(1);
                                	}
					requestsGranted++;
					grantedI++;
					int tempRec;
                                	switch (sndBuffer.intData)
                                	{
                                        	case -10:
                                        	        processTable[pos].r0++;
                                        	        recs.R0--;
							tempRec = 0;
                                        	        break;
                                        	case -11:
                                        	        processTable[pos].r1++;
                                        	        recs.R1--;
							tempRec = 1;
                                        	        break;
                                        	case -12:
                                        	        processTable[pos].r2++;
                                        	        recs.R2--;
							tempRec = 2;
                                        	        break;
                                        	case -13:
                                        	        processTable[pos].r3++;
                                        	        recs.R3--;
							tempRec = 3;
                                        	        break;
                                        	case -14:
                                        	        processTable[pos].r4++;
                                        	        recs.R4--;
							tempRec = 4;
                                        	        break;
                                        	case -15:
                                        	        processTable[pos].r5++;
                                        	        recs.R5--;
							tempRec = 5;
                                        	        break;
                                        	case -16:
                                        	        processTable[pos].r6++;
                                        	        recs.R6--;
							tempRec = 6;
                                        	        break;
                                        	case -17:
                                        	        processTable[pos].r7++;
                                        	        recs.R7--;
							tempRec = 7;
                                        	        break;
                                        	case -18:
                                        	        processTable[pos].r8++;
                                        	        recs.R8--;
							tempRec = 8;
                                        	        break;
                                        	case -19:
                                        	        processTable[pos].r9++;
                                        	        recs.R9--;
							tempRec = 9;
                                        	        break;
                                	}
					fprintf(logFile, "MASTER: %d requests resource %d at %d sec %d nano\n", sndBuffer.mtype, tempRec, *sysSec, *sysNano);
					printf("MASTER: %d requests resource %d at %d sec %d nano\n", sndBuffer.mtype, tempRec, *sysSec, *sysNano);
				}
				else
				{
					if (verboseCheck)
					{
						fprintf(logFile, "MASTER: %d added to blockedQ at %d sec %d nano\n", sndBuffer.mtype, *sysSec, *sysNano);
						printf("MASTER: %d added to blockedQ at %d sec %d nano\n", sndBuffer.mtype, *sysSec, *sysNano);
					}
				}
			}
			//then otherwise check if it's returning a resource
			else if (rcvBuffer.intData >= 10 && rcvBuffer.intData <= 19)
			{
				int myRec = 0;
				switch (rcvBuffer.intData)
				{
					case 10:
						sndBuffer.intData = 10;
						processTable[pos].r0--;
						recs.R0++;
						myRec = 0;
						break;
					case 11:
						sndBuffer.intData = 11;
                                                processTable[pos].r1--;
                                                recs.R1++;
						myRec = 1;
                                                break;
					case 12:
						sndBuffer.intData = 12;
                                                processTable[pos].r2--;
                                                recs.R2++;
						myRec = 2;
                                                break;
					case 13:

						sndBuffer.intData = 13;
                                                processTable[pos].r3--;
                                                recs.R3++;
						myRec = 3;
                                                break;
					case 14:
						sndBuffer.intData = 14;
                                                processTable[pos].r4--;
                                                recs.R4++;
						myRec = 4;
                                                break;
					case 15:
						sndBuffer.intData = 15;
                                                processTable[pos].r5--;
                                                recs.R5++;
						myRec = 5;
                                                break;
					case 16:
						sndBuffer.intData = 16;
                                                processTable[pos].r6--;
                                                recs.R6++;
						myRec = 6;
                                                break;
					case 17:
						sndBuffer.intData = 17;
                                                processTable[pos].r7--;
                                                recs.R7++;
						myRec = 7;
                                                break;
					case 18:
						sndBuffer.intData = 18;
                                                processTable[pos].r8--;
                                                recs.R8++;
						myRec = 8;
                                                break;
					case 19:
						sndBuffer.intData = 19;
                                                processTable[pos].r9--;
                                                recs.R9++;
						myRec = 9;
                                                break;
				}
				fprintf(logFile, "MASTER: %d returning resource %d at %d sec %d nano\n", sndBuffer.mtype, myRec, *sysSec, *sysNano);
				printf("MASTER: %d returning resource %d at %d sec %d nano\n", sndBuffer.mtype, myRec, *sysSec, *sysNano);

				//send message back, verifying that you received resource from child
				if (msgsnd(msqid, &sndBuffer, sizeof(msgbuffer) - sizeof(long), 0) == -1)
				{
					printf("issue sending from parentside\n");
					exit(1);
				}
				
			}
			//if 0, means worker is terminating so release its resources, remove from table and wait
			else if (rcvBuffer.intData == 0)
			{
				//free up all resources
				recs.R0 += processTable[pos].r0;
				recs.R1 += processTable[pos].r1;
				recs.R2 += processTable[pos].r2;
				recs.R3 += processTable[pos].r3;
				recs.R4 += processTable[pos].r4;
				recs.R5 += processTable[pos].r5;
				recs.R6 += processTable[pos].r6;
				recs.R7 += processTable[pos].r7;
				recs.R8 += processTable[pos].r8;
				recs.R9 += processTable[pos].r9;

				//set sndBuffer intData to 0
				sndBuffer.intData = 0;
				//send message to worker telling it to term
				if (msgsnd(msqid, &sndBuffer, sizeof(msgbuffer) - sizeof(long), 0) == -1)
				{
					printf("issue parentside sending term message\n");
					exit(1);
				}
				//log termination to child
                        	fprintf(logFile, "MASTER: pid %d released freeing up R0:%d R1:%d R2:%d R3:%d R4%d R5:%d R6:%d R7:%d R8:%d R9:%d\n", sndBuffer.mtype, processTable[pos].r0, processTable[pos].r1, processTable[pos].r2, processTable[pos].r3, processTable[pos].r4, processTable[pos].r5, processTable[pos].r6, processTable[pos].r7, processTable[pos].r8, processTable[pos].r9);
                        	printf("MASTER: pid %d released freeing up R0:%d R1:%d R2:%d R3:%d R4%d R5:%d R6:%d R7:%d R8:%d R9:%d\n", sndBuffer.mtype, processTable[pos].r0, processTable[pos].r1, processTable[pos].r2, processTable[pos].r3, processTable[pos].r4, processTable[pos].r5, processTable[pos].r6, processTable[pos].r7, processTable[pos].r8, processTable[pos].r9);

				//add to term, remove from table, wait, etc.
				terminatedChildren++;
				removeProcess(processTable, rcvBuffer.pid);
				wait(NULL);
				termS++;	
			}
			else
			{
				printf("error checking intData on parentSide\n");
				exit(1);
			}
		}
		//reset sndBuffer's intData for further comparison
		sndBuffer.intData = -1;

		//check if any resouces are able to be sent to something in a blocked queue, if so, send one resource per iteration
		//if-else instead of switch here since multiple could be true, but we'll just take the first
		if (r0bq.size() > 0 && recs.R0 > 0)
		{
			sndBuffer.intData = -10;
			sndBuffer.mtype = r0bq.front();
			r0bq.pop();
		}
		else if (r1bq.size() > 0 && recs.R1 > 0)
		{
			sndBuffer.intData = -11;
			sndBuffer.mtype = r1bq.front();
			r1bq.pop();	
		}
                else if (r2bq.size() > 0 && recs.R2 > 0)
                {
		        sndBuffer.intData = -12;
			sndBuffer.mtype = r2bq.front();
			r2bq.pop();
		}		
                else if (r3bq.size() > 0 && recs.R3 > 0)
                {
		        sndBuffer.intData = -13;
			sndBuffer.mtype = r3bq.front();
			r3bq.pop();
		}
                else if (r4bq.size() > 0 && recs.R4 > 0)
                {
		        sndBuffer.intData = -14;
			sndBuffer.mtype = r4bq.front();
			r4bq.pop();
		}
                else if (r5bq.size() > 0 && recs.R5 > 0)
                {
		        sndBuffer.intData = -15;
			sndBuffer.mtype = r5bq.front();
			r5bq.pop();
		}
                else if (r6bq.size() > 0 && recs.R6 > 0)
                {
		        sndBuffer.intData = -16;
			sndBuffer.mtype = r6bq.front();
			r6bq.pop();
		}
                else if (r7bq.size() > 0 && recs.R7 > 0)
                {
		        sndBuffer.intData = -17;
			sndBuffer.mtype = r7bq.front();
			r7bq.pop();
		}
                else if (r8bq.size() > 0 && recs.R8 > 0)
                {
		        sndBuffer.intData = -18;
			sndBuffer.mtype = r8bq.front();
			r8bq.pop();	
		}
                else if (r9bq.size() > 0 && recs.R9 > 0)
                {
		        sndBuffer.intData = -19;
			sndBuffer.mtype = r9bq.front();
			r9bq.pop();
		}

		//if sndBuffer isn't -1, then blockedQ exists & relative resources are available to provide for item in Q, so send message
		if (sndBuffer.intData != -1)
		{
                        if (msgsnd(msqid, &sndBuffer, sizeof(msgbuffer) - sizeof(long), 0) == -1)
                        {
                                printf("sending issue from parentside\n");
                        	exit(1);
                	}
			if (verboseCheck)
			{
				//switch to find out what resource was sent to blocked
				int tempRec = 0;
				switch (sndBuffer.intData)
				{
					case -10:
						tempRec = 0;
					case -11:
						tempRec = 1;
					case -12:
						tempRec = 2;
					case -13:
						tempRec = 3;
					case -14:
						tempRec = 4;
					case -15:
						tempRec = 5;
					case -16:
						tempRec = 6;
					case -17:
						tempRec = 7;
					case -18:
						tempRec = 8;
					case -19:
						tempRec = 9;
				}
			
				printf("MASTER: %d removed from blockedQ at %d sec %d nano, process receiving R%d\n", sndBuffer.mtype, *sysSec, *sysNano, tempRec);
				fprintf(logFile, "MASTER: %d removed from blockedQ at %d sec %d nano\n", sndBuffer.mtype, *sysSec, *sysNano);
			}
			int pos = findPos(processTable, sndBuffer.mtype);
			processTable[pos].blocked = -1;
			grantedW++;
			requestsGranted++;

			//alloc resource to recently unblocked process and remove resource from available
			switch (sndBuffer.intData)
			{
				case -10:
					processTable[pos].r0++;
					recs.R0--;
					break;
                                case -11:
                                        processTable[pos].r1++;
                                        recs.R1--;
                                        break;
                                case -12:
                                        processTable[pos].r2++;
                                        recs.R2--;
                                        break;
                                case -13:
                                        processTable[pos].r3++;
                                        recs.R3--;
                                        break;
                                case -14:
                                        processTable[pos].r4++;
                                        recs.R4--;
                                        break;
                                case -15:
                                        processTable[pos].r5++;
                                        recs.R5--;
                                        break;
                                case -16:
                                        processTable[pos].r6++;
                                        recs.R6--;
                                        break;
                                case -17:
                                        processTable[pos].r7++;
                                        recs.R7--;
                                        break;
                                case -18:
                                        processTable[pos].r8++;
                                        recs.R8--;
                                        break;
                                case -19:
                                        processTable[pos].r9++;
                                        recs.R9--;
                                        break;
			}
		}

		//declare vector for deadlocked pids then run deadlock detection, return vector of pids.
		queue<int> deadlockedPids;
		if (*sysSec > lastDetect)
		{
			fprintf(logFile, "MASTER: Running deadlock detection at %d sec %d nano\n", *sysSec, *sysNano);
			printf("MASTER: Running deadlock detection at %d sec %d nano\nPROCESS TABLE BEFORE CHECK:\n", *sysSec, *sysNano);
			printProcTable(processTable, *sysSec, *sysNano, logFile);
			timesR++;
			lastDetect++;
			deadlockedPids = detectDeadlock(processTable, recs);
		}
		//remove from table, add back to resources, add to term children, send messages for each pid, remove from blockedQ, wait for each if deadlock was detected
		int qSize = deadlockedPids.size();
		bool qSizeFlag = false;
		if (qSize > 0)
		{
			qSizeFlag = true;
			fprintf(logFile, "MASTER: At %d sec %d nano deadlock detected - current resources: R0:%d R1:%d R2:%d R3:%d R4:%d R5:%d R6:%d R7:%d R8:%d R9:%d\n", *sysSec, *sysNano, recs.R0, recs.R1, recs.R2, recs.R3, recs.R4, recs.R5, recs.R6, recs.R7, recs.R8, recs.R9);
			printf("MASTER: At %d sec %d nano Deadlock detected - current resources: R0:%d R1:%d R2:%d R3:%d R4:%d R5:%d R6:%d R7:%d R8:%d R9:%d\n", *sysSec, *sysNano, recs.R0, recs.R1, recs.R2, recs.R3, recs.R4, recs.R5, recs.R6, recs.R7, recs.R8, recs.R9);
		}
		else if (!(qSize > 0) && *sysSec > lastDetect)
		{
			fprintf(logFile, "MASTER: At %d sec %d nano no deadlock detected\n", *sysSec, *sysNano);
			printf("MASTER: At %d sec %d nano no deadlock detected\n", *sysSec, *sysNano);
		}
		for (int i = 0 ; i < qSize ; i++)
		{
			//get pos in procTable of dl'd proc
			int pos = findPos(processTable, deadlockedPids.front());

			//deadlock detected, write to file and term
			fprintf(logFile, " pid %d terminated freeing up R0:%d R1:%d R2:%d R3:%d R4:%d R5:%d R6:%d R7:%d R8:%d R9:%d\n", deadlockedPids.front(), processTable[pos].r0, processTable[pos].r1, processTable[pos].r2, processTable[pos].r3, processTable[pos].r4, processTable[pos].r5, processTable[pos].r6, processTable[pos].r7, processTable[pos].r8, processTable[pos].r9);
			printf(" pid %d terminated freeing up R0:%d R1:%d R2:%d R3:%d R4:%d R5:%d R6:%d R7:%d R8:%d R9:%d\n", deadlockedPids.front(), processTable[pos].r0, processTable[pos].r1, processTable[pos].r2, processTable[pos].r3, processTable[pos].r4, processTable[pos].r5, processTable[pos].r6, processTable[pos].r7, processTable[pos].r8, processTable[pos].r9);	
			
			//add resources back from term'd proc
                        recs.R0 += processTable[pos].r0;
                        recs.R1 += processTable[pos].r1;
                        recs.R2 += processTable[pos].r2;
                        recs.R3 += processTable[pos].r3;
                        recs.R4 += processTable[pos].r4;
                        recs.R5 += processTable[pos].r5;
                        recs.R6 += processTable[pos].r6;
                        recs.R7 += processTable[pos].r7;
                        recs.R8 += processTable[pos].r8;
                        recs.R9 += processTable[pos].r9;

			//remove relative proc from blocked queue
			switch (processTable[pos].blocked)
			{
				case 0:
					r0bq = clearBlockedProcess(r0bq, deadlockedPids.front());
					break;
				case 1:
                        		r1bq = clearBlockedProcess(r1bq, deadlockedPids.front());
					break;
				case 2:
                        		r2bq = clearBlockedProcess(r2bq, deadlockedPids.front());
					break;
				case 3:
                        		r3bq = clearBlockedProcess(r3bq, deadlockedPids.front());
					break;
				case 4:
                        		r4bq = clearBlockedProcess(r4bq, deadlockedPids.front());
					break;
				case 5:
                        		r5bq = clearBlockedProcess(r5bq, deadlockedPids.front());
					break;
				case 6:
                        		r6bq = clearBlockedProcess(r6bq, deadlockedPids.front());
					break;
				case 7:
                        		r7bq = clearBlockedProcess(r7bq, deadlockedPids.front());
					break;
				case 8:
                        		r8bq = clearBlockedProcess(r8bq, deadlockedPids.front());
					break;
				case 9:
                        		r9bq = clearBlockedProcess(r9bq, deadlockedPids.front());
					break;
			}
			//send termination message to relative process
                        sndBuffer.mtype = deadlockedPids.front();
                        sndBuffer.intData = 0;
                        if (msgsnd(msqid, &sndBuffer, sizeof(msgbuffer) - sizeof(long), 0) == -1)
                        {
                        	printf("sending issue from parentside\n");
                        	exit(1);
                        }
			//increment number of term'd children, remove process from procTable and wait()
                        terminatedChildren++;
                        removeProcess(processTable, deadlockedPids.front());
                        wait(NULL);
			deadlockedPids.pop();
			termD++;
		}
		if (qSizeFlag == true)
		{
			fprintf(logFile, "MASTER: deadlock cleared - current resources: R0:%d R1:%d R2:%d R3:%d R4:%d R5:%d R6:%d R7:%d R8:%d R9:%d\n", recs.R0, recs.R1, recs.R2, recs.R3, recs.R4, recs.R5, recs.R6, recs.R7, recs.R8, recs.R9);
			printf("MASTER: deadlock cleared - current resources: R0:%d R1:%d R2:%d R3:%d R4:%d R5:%d R6:%d R7:%d R8:%d R9:%d\n", recs.R0, recs.R1, recs.R2, recs.R3, recs.R4, recs.R5, recs.R6, recs.R7, recs.R8, recs.R9);
		}
	}

	//calculate and print relative statistics
	double tempTally = 0;
	for (int i = 0 ; i < allTermPer.size() ; i++)
		tempTally += allTermPer[i];
	avgTermPer = tempTally / allTermPer.size();

	fprintf(logFile, "\nIMMEDIATE REQUESTS GRANTED: %d\t\tREQUESTS GRANTED AFTER WAIT: %d\n", grantedI, grantedW);
	printf("\nIMMEDIATE REQUESTS GRANTED: %d\t\tREQUESTS GRANTED AFTER WAIT: %d\n", grantedI, grantedW);
	
	printf("PROCESSES TERMINATED SUCCESSFULLY: %d\tPROCESSES TERMINATED THROUGH DLD/DLR: %d\n", termS, termD);	
	if (allTermPer.size() > 0)
	{
		fprintf(logFile, "AVERAGE PERCENTAGE OF PROCESSES ELIMINATED THROUGH DL/DR WHEN DL LOCATED: %f\%\n", avgTermPer * 100);
		printf("AVERAGE PERCENTAGE OF PROCESSES ELIMINATED THROUGH DL/DR WHEN DL LOCATED: %f\%\n", avgTermPer * 100);
	}
	else
	{
		fprintf(logFile, "AVERAGE PERCENTAGE OF PROCESSES ELIMINATED THROUGH DL/DR WHEN DL LOCATED: NA - NO DEADLOCKS\n");
		printf("AVERAGE PERCENTAGE OF PROCESSES ELIMINATED THROUGH DL/DR WHEN DL LOCATED: NA - NO DEADLOCKS\n");
	}
	fprintf(logFile, "TOTAL NUMBER OF TIMES DEADLOCK DETECTION RAN: %d\n", timesR);
	printf("TOTAL NUMBER OF TIMES DEADLOCK DETECTION RAN: %d\n", timesR);
	printf("\nTABLE AT TERMINATION:\n");
	fprintf(logFile, "TABLE AT TERMINATION:\n");
	printProcTable(processTable, *sysSec, *sysNano, logFile);
	
	//get rid of sysV msq
	if (msgctl(msqid, IPC_RMID, NULL) == -1)
	{
		printf("issue killing mq on parentside\n");
		exit(1);
	}

	//unlink shared memory
	shm_unlink("/secMemSeg");
	shm_unlink("/nanoMemSeg");

	//close logfile
	fclose(logFile);

        return 0;
}

//function to loop through process table and print it out in a more readable manner
//fileprints are just commented out incase you want them in your file
void printProcTable(struct PCB procTable[], int sec, int nano, FILE* logFile)
{
	printf("OSS PID:%d SysClock: %d SysClockNano: %d\n", getpid(), sec, nano);
	fprintf(logFile, "OSS PID:%d SysClock: %d SysClockNano: %d\n", getpid(), sec, nano);
	printf("Process Table:\n");
	fprintf(logFile, "Process Table:\n");
	printf("Entry\tOccupied PID\tR0\tR1\tR2\tR3\tR4\tR5\tR6\tR7\tR8\tR9\tblockedRec\tStartS\tStartN\n");
	fprintf(logFile,"Entry\tOccupied PID\tR0\tR1\tR2\tR3\tR4\tR5\tR6\tR7\tR8\tR9\tblockedRec\tStartS\tStartN\n");
	for (int i = 0 ; i < 18 ; i++)
	{
		printf("%d\t %d\t %d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t\t%d\t%d\n", i, procTable[i].occupied, procTable[i].pid, procTable[i].r0, procTable[i].r1, procTable[i].r2, procTable[i].r3, procTable[i].r4, procTable[i].r5, procTable[i].r6, procTable[i].r7, procTable[i].r8, procTable[i].r9, procTable[i].blocked, procTable[i].startSec, procTable[i].startNano);
		fprintf(logFile, "%d\t %d\t %d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t\t%d\t%d\n", i, procTable[i].occupied, procTable[i].pid, procTable[i].r0, procTable[i].r1, procTable[i].r2, procTable[i].r3, procTable[i].r4, procTable[i].r5, procTable[i].r6, procTable[i].r7, procTable[i].r8, procTable[i].r9, procTable[i].blocked, procTable[i].startSec, procTable[i].startNano);
	}
}

//function that loops through the process table to find a relative pid and remove it's line while moving the rest back together
void removeProcess(struct PCB procTable[], int pid)
{
	bool found = false;
	for (int i = 0 ; i < 18 ; i++)
	{
		if (procTable[i].pid == pid)
		{
			//if assessing the final position of the array, zero it out
			if (i == 17)
			{
				procTable[i].occupied = 0;
				procTable[i].pid = 0;
				procTable[i].startSec = 0;
				procTable[i].startNano = 0;
				procTable[i].r0 = 0;
				procTable[i].r1 = 0;
				procTable[i].r2 = 0;
				procTable[i].r3 = 0;
				procTable[i].r4 = 0;
				procTable[i].r5 = 0;
				procTable[i].r6 = 0;
				procTable[i].r7 = 0;
				procTable[i].r8 = 0;
				procTable[i].r9 = 0;
				procTable[i].blocked = -1;
				return;
			}
			//otherwise, set the positional value equal to the next indexed value, moves everything to the "left" by one.
			else
			{
				for (int j = i ; j < 17 ; j++)
				{
					procTable[j].occupied = procTable[j + 1].occupied;
					procTable[j].pid = procTable[j + 1].pid;
					procTable[j].startSec = procTable[j + 1].startSec;
					procTable[j].startNano = procTable[j + 1].startNano;
					procTable[j].r0 = procTable[j + 1].r0;
					procTable[j].r1 = procTable[j + 1].r1;
					procTable[j].r2 = procTable[j + 1].r2;
					procTable[j].r3 = procTable[j + 1].r3;
					procTable[j].r4 = procTable[j + 1].r4;
					procTable[j].r5 = procTable[j + 1].r5;
					procTable[j].r6 = procTable[j + 1].r6;
					procTable[j].r7 = procTable[j + 1].r7;
					procTable[j].r8 = procTable[j + 1].r8;
					procTable[j].r9 = procTable[j + 1].r9;
					procTable[j].blocked = procTable[j + 1].blocked;					
				}
				procTable[17].occupied = 0;
				procTable[17].pid = 0;
				procTable[17].startSec = 0;
				procTable[17].startNano = 0;
				procTable[17].r0 = 0;
				procTable[17].r1 = 0;
				procTable[17].r2 = 0;
				procTable[17].r3 = 0;
				procTable[17].r4 = 0;
				procTable[17].r5 = 0;
				procTable[17].r6 = 0;
				procTable[17].r7 = 0;
				procTable[17].r8 = 0;
				procTable[17].r9 = 0;
				procTable[17].blocked = -1;
				return;
			}
		}
	}
}

//function to go to lowest empty position in the process table and enter the relative child information
void addProcess(struct PCB procTable[], int pid, int sec, int nano)
{
	for (int i = 0 ; i < 18 ; i++)
	{
		if (procTable[i].occupied == 0)
		{
			procTable[i].occupied = 1;
			procTable[i].pid = pid;
			procTable[i].startSec = sec;
			procTable[i].startNano = nano;
			return;
		}
	}
}
