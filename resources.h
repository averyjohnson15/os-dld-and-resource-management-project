#include <stdlib.h>
#include <stdio.h>

#ifndef RESOURCES_H
#define RESOURCES_H

#define RESOURCE_MAX 20

struct resources
{
	int R0;
	int R1;
	int R2;
	int R3;
	int R4;
	int R5;
	int R6;
	int R7;
	int R8;
	int R9;
} resources;

#endif
