//for information on program, see README or header comment on oss.cpp

//preprocessor directives
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <mqueue.h>
#include <ctime>
#include <sys/ipc.h>
#include <sys/msg.h>

#include "resources.h"

using namespace std;

//macro and typedef to be used for sysV queue
#define PERMS 0644
typedef struct msgbuffer
{
	long mtype;
	int intData;
	int pid;
} msgbuffer;

//worker entry
int main(int argc, char* argv[])
{
	//initialize resources at subprocess at 0 each
	struct resources recs;
	recs.R0 = 0;
	recs.R1 = 0;
	recs.R2 = 0;
	recs.R3 = 0;
	recs.R4 = 0;
	recs.R5 = 0;
	recs.R6 = 0;
	recs.R7 = 0;
	recs.R8 = 0;
	recs.R9 = 0;

	//declare resources for mqueue
	msgbuffer sndBuffer, rcvBuffer;
	int msqid = 0;
	key_t key;

	//ftok to get key for queue
	if ((key = ftok("msgq.txt", 1)) == -1)
	{
		printf("issue with ftok childside\n");
		exit(1);
	}

	//join msg queue that parent created
	if ((msqid = msgget(key, PERMS)) == -1)
	{
		printf("issue connecting to queue on childside\n");
		exit(1);
	}	

        //open shared mem sec and nano vars
        //set up sec destination, throw error if issue occurs
        int secMemSeg = shm_open("/secMemSeg", O_RDONLY, 0777);
        if (secMemSeg < 0)
        {
                printf("issue creating sec mem seg\n");
                exit(1);
        }
        //configure size of sec mem seg
        ftruncate(secMemSeg, sizeof(int));
        //use mmap to map sec address, throw relative error if issue occurs
        void* secAddress = mmap(NULL, sizeof(int), PROT_READ, MAP_SHARED, secMemSeg, 0);
        if (secAddress == MAP_FAILED)
        {
                printf("issue setting up sec address\n");
                exit(1);
        }
        //set that address as the value of a pointer to the sec int value
        int* sysSec = (int*)secAddress;

        //set up nano mem seg, throw error if issue occurs
        int nanoMemSeg = shm_open("/nanoMemSeg", O_RDONLY, 0777);
        if (nanoMemSeg < 0)
        {
                printf("Issue creating nano mem seg\n");
                exit(1);
        }
        //configure size of nano mem seg
        ftruncate(nanoMemSeg, sizeof(int));
        //use mmap to map nano address, throw relative error if issue occurs
        void* nanoAddress = mmap(NULL, sizeof(int), PROT_READ, MAP_SHARED, nanoMemSeg, 0);
        if (nanoAddress == MAP_FAILED)
        {
                printf("issue setting up nano address\n");
                exit(1);
        }
        //set shared address as value of pointer to the nano int value
        int* sysNano = (int*)nanoAddress;

	//seed the rng and create max time between subprocess relays back to oss
	srand(time(0));
	int mtbSec = 0;
	int mtbNano = 250000000;

	//create next time for subprocess to send message to oss
	int niSec = rand() % (mtbSec + 1);
	int niNano = 0;
	if (niSec == mtbSec)
		niNano = rand() % (mtbNano + 1);
	else
		//should be (999999999 + 1) if mtbSec is above 0
		niNano = rand() % (250000000 + 1);
	int ntfSec = (*sysSec) + niSec;
	int ntfNano = (*sysNano) + niNano;

	//set start sec and nano for process
	int startSec = *sysSec;
	int startNano = *sysNano;
	bool secondPassed = false;
	bool tenSecPassed = false;
	bool twentySecPassed = false;
	bool thirtySecPassed = false;

	//loop through receiving and sending messages to/from OSS until completion
	while (true)
	{
		//check and see if a second has passed in order to start checking for termination
		if (!secondPassed && (*sysSec > (startSec + 1) || (*sysSec > startSec && *sysNano >= startNano)))
			secondPassed = true;
		if (!tenSecPassed && (*sysSec > (startSec + 10) || (*sysSec > (startSec + 9) && *sysNano >= startNano)))
			tenSecPassed = true;
		if (!twentySecPassed && (*sysSec > (startSec + 20) || (*sysSec > (startSec + 19) && *sysNano >= startNano)))
			twentySecPassed = true;
		if (!thirtySecPassed && (*sysSec > (startSec + 30) || (*sysSec > (startSec + 29) && *sysNano >= startNano)))
			thirtySecPassed = true;

		//declare rand/return vars to be used in return calcs
		int returnValue = 0;
		int tempRand = rand() % 100 + 1;
		bool requestFlag = false;
		bool returnFlag = false;
		bool termFlag = false;

		//if next interval time has passed, make request/return/terminate to oss and reroll time, only term if sec >= 1
		if (*sysSec > ntfSec || *sysSec == ntfSec && *sysNano >= ntfNano)
		{
			//calc new time
			if (niSec == mtbSec)
				niNano = rand() % (mtbNano + 1);
			else
				niNano = rand() % (250000000 + 1);
			ntfSec = (*sysSec) + niSec;
			ntfNano = (*sysNano) + niNano;
			if (ntfNano >= 1000000000)
			{
				ntfSec++;
				ntfNano -= 1000000000;
			}

			//calc whether it will be term, request or return.
			
			//COMMENTED OUT AGING ELEMENT. STILL HERE FOR TESTING, ADD ELSE BEFORE 'secondPassed' CONDITIONAL IF UNCOMMENTED
			/*
			if (thirtySecPassed)
			{
				if (tempRand <= 65)
					requestFlag = true;
				else if (tempRand <= 70)
					returnFlag = true;
				else
					termFlag = true;
			}
			else if (twentySecPassed)
			{
				if (tempRand <= 75)
					requestFlag = true;
				else if (tempRand <= 80)
					returnFlag = true;
				else
					termFlag = true;
			}
			else if (tenSecPassed)
			{
				if (tempRand <= 85)
					requestFlag = true;
				else if (tempRand <= 90)
					returnFlag = true;
				else
					termFlag = true;
			}
			*/
			if (secondPassed)
			{
				if (tempRand <= 95)
					requestFlag = true;
				else if (tempRand <= 99)
					returnFlag = true;
				else
					termFlag = true;
			}
			else
			{
				if (tempRand <= 96)
					requestFlag = true;
				else
					returnFlag = true;
			}

			//roll random resource 0-9
			int randResource = rand() % (9 + 1);

			//flag to keep track of whether or not a message needs to be sent
			bool needSent = false;

			//if requestFlag is true, request a variable if possible
			if (requestFlag)
			{
				switch(randResource)
				{
					case 0:
						if ((RESOURCE_MAX - recs.R0) > 0)
						{
							sndBuffer.intData = -10;
							needSent = true;
						}
						break;
					case 1:
                                                if ((RESOURCE_MAX - recs.R1) > 0)
                                                {
                                                        sndBuffer.intData = -11;
                                                        needSent = true;
                                                }
						break;
					case 2:
                                                if ((RESOURCE_MAX - recs.R2) > 0)
                                                {
                                                        sndBuffer.intData = -12;
                                                        needSent = true;
                                                }
						break;
					case 3:
                                                if ((RESOURCE_MAX - recs.R3) > 0)
                                                {
                                                        sndBuffer.intData = -13;
                                                        needSent = true;
                                                }
						break;
					case 4:
                                                if ((RESOURCE_MAX - recs.R4) > 0)
                                                {
                                                        sndBuffer.intData = -14;
                                                        needSent = true;
                                                }
						break;
					case 5:
                                                if ((RESOURCE_MAX - recs.R5) > 0)
                                                {
                                                        sndBuffer.intData = -15;
                                                        needSent = true;
                                                }
						break;
					case 6:
                                                if ((RESOURCE_MAX - recs.R6) > 0)
                                                {
                                                        sndBuffer.intData = -16;
                                                        needSent = true;
                                                }
						break;
					case 7:
                                                if ((RESOURCE_MAX - recs.R7) > 0)
                                                {
                                                        sndBuffer.intData = -17;
                                                        needSent = true;
                                                }
						break;
					case 8:
                                                if ((RESOURCE_MAX - recs.R8) > 0)
                                                {
                                                        sndBuffer.intData = -18;
                                                        needSent = true;
                                                }
						break;
					case 9:
                                                if ((RESOURCE_MAX - recs.R9) > 0)
                                                {
                                                        sndBuffer.intData = -19;
                                                        needSent = true;
                                                }
						break;
				}
			}

			//else if return Flag is true and it's possible, return resource
                        if (returnFlag)
                        {
                                switch(randResource)
                                {
                                        case 0:
                                                if (recs.R0 > 0)
                                                {
                                                        sndBuffer.intData = 10;
                                                        needSent = true;
                                                }
                                                break;
                                        case 1:
                                                if (recs.R1 > 0)
                                                {
                                                        sndBuffer.intData = 11;
                                                        needSent = true;
                                                }
                                                break;
                                        case 2:
                                                if (recs.R2 > 0)
                                                {
                                                        sndBuffer.intData = 12;
                                                        needSent = true;
                                                }
                                                break;
                                        case 3:
                                                if (recs.R3 > 0)
                                                {
                                                        sndBuffer.intData = 13;
                                                        needSent = true;
                                                }
                                                break;
                                        case 4:
                                                if (recs.R4 > 0)
                                                {
                                                        sndBuffer.intData = 14;
                                                        needSent = true;
                                                }
                                                break;
                                        case 5:
                                                if (recs.R5 > 0)
                                                {
                                                        sndBuffer.intData = 15;
                                                        needSent = true;
                                                }
                                                break;
                                        case 6:
                                                if (recs.R6 > 0)
                                                {
                                                        sndBuffer.intData = 16;
                                                        needSent = true;
                                                }
                                                break;
                                        case 7:
                                                if (recs.R7 > 0)
                                                {
                                                        sndBuffer.intData = 17;
                                                        needSent = true;
                                                }
                                                break;
                                        case 8:
                                                if (recs.R8 > 0)
                                                {
                                                        sndBuffer.intData = 18;
                                                        needSent = true;
                                                }
                                                break;
                                        case 9:
                                                if (recs.R9 > 0)
                                                {
                                                        sndBuffer.intData = 19;
                                                        needSent = true;
                                                }
                                                break;
                                }
                        }	
			//else termFlag is true, return term message to oss
			else if (termFlag)
			{
				sndBuffer.intData = 0;
				needSent = true;
			}

			//if a message needs to be sent, send the message to OSS
			if (needSent)
			{
				//set rest of sndBuffer attributes
				sndBuffer.mtype = getppid();
				sndBuffer.pid = getpid();

				//attempt to send message to OSS for request, term or return
				if (msgsnd(msqid, &sndBuffer, sizeof(msgbuffer) - sizeof(long), 0) == -1)
				{
					printf("Issue sending message from childside\n");
					exit(1);
				}

				//reset rcvBuffer.intData before checking (just for testing purposes)
				rcvBuffer.intData = -2;
				
				//only set to rcv message if asking for resource
				//setup for receive. Block process until it receives message
				if (msgrcv(msqid, &rcvBuffer, sizeof(msgbuffer), getpid(), 0) == -1)
				{
					printf("Issue receiving message on childside\n");
					exit(1);
				}
				//depending on response from oss, respond accordingly
				switch(rcvBuffer.intData)
				{
					case -1://test case shouldn't reach
						printf("-1 reached unexpectedly\n");
						exit(1);
						break;
					case -2://test case 2 shouldn't reach
						printf("-2 reached unexpectedly\n");
						printf("Issue receiving message!\n");
						exit(1);
						break;
					case 0://self term due to deadlock
						exit(0);
						break;
					case -10:
						recs.R0++;
						break;
                                        case -11:
                                                recs.R1++;
                                                break;
                                        case -12:
                                                recs.R2++;
                                                break;
                                        case -13:
                                                recs.R3++;
                                                break;
                                        case -14:
                                                recs.R4++;
                                                break;
                                        case -15:
                                                recs.R5++;
                                                break;
                                        case -16:
                                                recs.R6++;
                                                break;
                                        case -17:
                                                recs.R7++;
                                                break;
                                        case -18:
                                                recs.R8++;
                                                break;
                                        case -19:
                                                recs.R9++;
                                                break;
                                        case 10:
                                                recs.R0--;
                                                break;
                                        case 11:
                                                recs.R1--;
                                                break;
                                        case 12:
                                                recs.R2--;
                                                break;
                                        case 13:
                                                recs.R3--;
                                                break;
                                        case 14:
                                                recs.R4--;
                                                break;
                                        case 15:
                                                recs.R5--;
                                                break;
                                        case 16:
                                                recs.R6--;
                                                break;
                                        case 17:
                                                recs.R7--;
                                                break;
                                        case 18:
                                                recs.R8--;
                                                break;
                                        case 19:
                                                recs.R9--;
                                                break;
				}
			}
		}

	}		
        return 0;
}
